package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public String checkLogin(final String login, final String password) {
        String status = "UNATHORIZED";

        for(Map.Entry<Integer, User> user : usersDatabase.entrySet()) {
            if (user.getValue().getLogin().equals(login)) {
                if (user.getValue().isActive() == false) {
                    status = "FORBIDDEN";
                }
                if (user.getValue().getPassword().equals(password)) {
                    status = "OK";
                } else {
                    if (user.getValue().getIncorrectLoginCounter() > 3) {
                        user.getValue().setIsActiveFalse();
                    } else {
                        user.getValue().setIncorrectLoginCounter(1);
                    }
                }
            }
        }
        return status;
    }
}
